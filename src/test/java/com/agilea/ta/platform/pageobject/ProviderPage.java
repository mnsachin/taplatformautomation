package com.agilea.ta.platform.pageobject;

public class ProviderPage {
	
	public static String select_The_ProviderOrServiceType_HLBL = "//h2[contains(text(),'Enter the provider or service type.')]~XPATH";
	public static String continue_BTN = "//div[@class='buttons-Container']//button[contains(text(),'Continue')]~XPATH";
	public static String goBack_BTN = "goBack~ID";
	public static String providerType_TXT = "providerType~ID";
	public static String providerType_Suggestion_Listbox_ELM = "//div[@class='suggestion-list']/ul[@role='listbox']~XPATH";
	public static String providerType_Suggestion_ListItems_ELM = "//div[@class='suggestion-list']/ul[@role='listbox']/li~XPATH";
	public static String providerType_Suggestion_ListItem_ELM = "//div[@class='suggestion-list']/ul[@role='listbox']/li[providerindex]~XPATH";
	public static String providerType_Suggestion_ListItem_Selected_ELM = "//div[@class='suggestion-list']/ul[@role='listbox']/li[@class='suggestion-list-item selected']~XPATH";
	public static String providerType_Close_BTN = "//div[@class='suggestion-text-box']//span[@class='clear-icon']~XPATH";
	public static String providerType_Select_ELM = "//li[contains(@class,'suggestion-list-item') and contains(text(),'providertype')]~XPATH";
	public static String clearProviderType_BTN = "//span[@class='clear-icon']~XPATH";
	public static String noProviderType_LBL = "//li[@class='suggestion-list-error']/div[contains(text(),\"We can\'t find the exact provider type you searched for.\")]~XPATH";
	public static String noProviderType_LNK = "//li[@class='suggestion-list-error']/div[@class='message']/a~XPATH";
	public static String previouslySelectedProviders_HLBL = "//h2[contains(text(),'Previously selected providers')]~XPATH";
}
