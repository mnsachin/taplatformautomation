package com.agilea.ta.platform.pageobject;

public class CommonPage {

	public static String manulifeLogo_IMG = "//img[@class='manulife-logo']~XPATH";
	
	//Navigation Label
	public static String benefit_ELM = "//div[contains(text(),'Benefit')]~XPATH";
	public static String provider_ELM = "//div[contains(text(),'Provider')]~XPATH";
	public static String expenses_ELM = "//div[contains(text(),'Expenses')]~XPATH";
	public static String submit_ELM = "//div[contains(text(),'Submit')]~XPATH";
	
	public static String benefit_Mobile_ELM = "//div[@class='d-block d-md-none mobile-text-placeholder' and contains(text(),'Benefit')]~XPATH";
	public static String provider_Mobile_ELM = "//div[@class='d-block d-md-none mobile-text-placeholder' and contains(text(),'Provider')]~XPATH";
	public static String expenses_Mobile_ELM = "//div[@class='d-block d-md-none mobile-text-placeholder' and contains(text(),'Expenses')]~XPATH";
	public static String submit_Mobile_ELM = "//div[@class='d-block d-md-none mobile-text-placeholder' and contains(text(),'Submit')]~XPATH";
	
	public static String benefit_Bolded_ELM = "//div[@class='step one' and @step-status='active']//div[@class='d-md-block d-none progressbar-text' and contains(text(),'Benefit')]~XPATH";
	public static String provider_Bolded_ELM = "//div[@class='step two' and @step-status='active']//div[@class='d-md-block d-none progressbar-text' and contains(text(),'Provider')]~XPATH";
	public static String expenses_Bolded_ELM = "//div[@class='step three' and @step-status='active']//div[@class='d-md-block d-none progressbar-text' and contains(text(),'Expenses')]~XPATH";
	public static String submit_Bolded_ELM = "//div[@class='step four' and @step-status='active']//div[@class='d-md-block d-none progressbar-text' and contains(text(),'Submit')]~XPATH";
	
	public static String benefit_NotBolded_ELM = "//div[@class='step one' and @step-status='inactive']//div[@class='d-md-block d-none progressbar-text' and contains(text(),'Benefit')]~XPATH";
	public static String provider_NotBolded_ELM = "//div[@class='step two' and @step-status='inactive']//div[@class='d-md-block d-none progressbar-text' and contains(text(),'Provider')]~XPATH";
	public static String expenses_NotBolded_ELM = "//div[@class='step three' and @step-status='inactive']//div[@class='d-md-block d-none progressbar-text' and contains(text(),'Expenses')]~XPATH";
	public static String submit_NotBolded_ELM = "//div[@class='step four' and @step-status='inactive']//div[@class='d-md-block d-none progressbar-text' and contains(text(),'Submit')]~XPATH";
	
	public static String benefit_CompletedAndNotBolded_ELM = "//div[@class='step one' and @step-status='completed']//div[@class='d-md-block d-none progressbar-text' and contains(text(),'Benefit')]~XPATH";
	public static String provider_CompletedAndNotBolded_ELM = "//div[@class='step two' and @step-status='completed']//div[@class='d-md-block d-none progressbar-text' and contains(text(),'Provider')]~XPATH";
	public static String expenses_CompletedAndNotBolded_ELM = "//div[@class='step three' and @step-status='completed']//div[@class='d-md-block d-none progressbar-text' and contains(text(),'Expenses')]~XPATH";
	public static String submit_CompletedAndNotBolded_ELM = "//div[@class='step four' and @step-status='completed']//div[@class='d-md-block d-none progressbar-text' and contains(text(),'Submit')]~XPATH";
	
	public static String step1_Highlighted_ELM = "//div[@class='step one' and @step-status='active']/div[@class='indicator']/span[contains(text(),'1')]~XPATH";
	public static String step2_Highlighted_ELM = "//div[@class='step two' and @step-status='active']/div[@class='indicator']/span[contains(text(),'2')]~XPATH";
	public static String step3_Highlighted_ELM = "//div[@class='step three' and @step-status='active']/div[@class='indicator']/span[contains(text(),'3')]~XPATH";
	public static String step4_Highlighted_ELM = "//div[@class='step four' and @step-status='active']/div[@class='indicator']/span[contains(text(),'4')]~XPATH";
	
	public static String step1_NotHighlighted_ELM = "//div[@class='step one' and @step-status='inactive']/div[@class='indicator']/span[contains(text(),'1')]~XPATH";
	public static String step2_NotHighlighted_ELM = "//div[@class='step two' and @step-status='inactive']/div[@class='indicator']/span[contains(text(),'2')]~XPATH";
	public static String step3_NotHighlighted_ELM = "//div[@class='step three' and @step-status='inactive']/div[@class='indicator']/span[contains(text(),'3')]~XPATH";
	public static String step4_NotHighlighted_ELM = "//div[@class='step four' and @step-status='inactive']/div[@class='indicator']/span[contains(text(),'4')]~XPATH";
	
	public static String step1_Completed_ELM = "//div[@class='step one' and @step-status='completed']/div[@class='indicator']/span[contains(text(),'1')]~XPATH";
	public static String step2_Completed_ELM = "//div[@class='step two' and @step-status='completed']/div[@class='indicator']/span[contains(text(),'2')]~XPATH";
	public static String step3_Completed_ELM = "//div[@class='step three' and @step-status='completed']/div[@class='indicator']/span[contains(text(),'3')]~XPATH";
	public static String step4_Completed_ELM = "//div[@class='step four' and @step-status='completed']/div[@class='indicator']/span[contains(text(),'4')]~XPATH";
	
	public static String step1_TickMark_ELM = "//div[@class='step one' and @step-status='completed']/div[@class='indicator']~XPATH";
	public static String step2_TickMark_ELM = "//div[@class='step two' and @step-status='completed']/div[@class='indicator']/span~XPATH";
	public static String step3_TickMark_ELM = "//div[@class='step three' and @step-status='completed']/div[@class='indicator']~XPATH";
	public static String step4_TickMark_ELM = "//div[@class='step four' and @step-status='completed']/div[@class='indicator']~XPATH";
	
	public static String progressBar_1_ELM = "//div[@class='row progress-navbar-inner']//div[@class='bar'][1]~XPATH";
	public static String progressBar_2_ELM = "//div[@class='row progress-navbar-inner']//div[@class='bar'][2]~XPATH";
	public static String progressBar_3_ELM = "//div[@class='row progress-navbar-inner']//div[@class='bar'][3]~XPATH";
	
	//Temporary login screen
	public static String login_HLBL = "//h5[contains(text(),'Login')]~XPATH";
	public static String temporaryLogin_MSG = "//h5[contains(text(),'This is a temporary page to simulate logging in to the system.')]~XPATH";
	public static String memberId_TXT = "exampleInputEmail1~ID";
	public static String setMemberId_BTN = "//div[@class='buttons-Container']/button[contains(text(),'Set Member ID') or class='btn btn-primary gb-button']~XPATH";
}
