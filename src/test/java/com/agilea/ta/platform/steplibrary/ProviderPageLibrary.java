package com.agilea.ta.platform.steplibrary;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.testng.Assert;

import com.agilea.ta.platform.framework.common.CommonLibrary;
import com.agilea.ta.platform.pageobject.ProviderPage;

import io.appium.java_client.AppiumDriver;

public class ProviderPageLibrary extends CommonLibrary {

	private static Logger logger = LogManager.getLogger(ProviderPageLibrary.class);

	public static void clicksButtonInProviderScreen(String buttonName) {
		try {
			switch (buttonName) {
			case "continue":
				click(ProviderPage.continue_BTN);
				break;
			case "go back":
				click(ProviderPage.goBack_BTN);
				break;
			default:
				Assert.assertFalse(true);
				throw new Exception("Input" + buttonName + "  is wrong");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyContinueButtonState(String continueButtonState) {
		try {
			switch (continueButtonState) {
			case "disabled":
				Assert.assertTrue(isElementDisplayed(ProviderPage.continue_BTN));
				Assert.assertFalse((findElement(ProviderPage.continue_BTN).isEnabled()));
				break;
			case "enabled":
				Assert.assertTrue(isElementDisplayed(ProviderPage.continue_BTN));
				Assert.assertTrue((findElement(ProviderPage.continue_BTN).isEnabled()));
				break;
			default:
				Assert.assertFalse(true);
				throw new Exception("Input" + continueButtonState + "  is wrong");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyDefaultTextInProviderTypeTextField(String defaultText) {
		try {
			Assert.assertEquals(getAttribute(ProviderPage.providerType_TXT, "placeholder"), defaultText);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyFieldStatusInProviderScreen(String fieldName, String fieldStatus) {
		String verifyFieldNameStatus_ELM = null;
		try {
			switch (fieldName) {
			case "provider type":
				verifyFieldNameStatus_ELM = ProviderPage.providerType_TXT;
				break;
			default:
				throw new Exception("Not able to find the " + fieldName + " field");
			}
			if (fieldStatus.equals("disabled"))
				Assert.assertFalse(isElementEnabled(verifyFieldNameStatus_ELM));
			else
				Assert.assertTrue(isElementEnabled(verifyFieldNameStatus_ELM));
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void clicksOnProviderTypeField() {
		try {
			click(ProviderPage.providerType_TXT);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void clicksOnCloseButtonInProviderTypeField() {
		try {
			click(ProviderPage.clearProviderType_BTN);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyTextinProviderTypeField(String providerType) {
		try {
			Assert.assertNotEquals(getText(ProviderPage.providerType_TXT), providerType);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyDefaultTextinProviderTypeField() {
		try {
			Assert.assertEquals(getText(ProviderPage.providerType_TXT), "");
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void enterTextOnProviderTypeField(String providerType) {
		try {
			type(ProviderPage.providerType_TXT, providerType);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void deleteTextOnProviderTypeField(String providerType) {
		try {
			for (int i = 1; i <= providerType.length(); i++)
				findElement(ProviderPage.providerType_TXT).sendKeys(Keys.BACK_SPACE);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyProviderTypeAutoSuggestion() {
		try {
			Assert.assertTrue(isElementDisplayed(ProviderPage.providerType_Suggestion_Listbox_ELM));
			for (int i = 0; i < getElementCount(ProviderPage.providerType_Suggestion_ListItems_ELM); i++) {
				find(ProviderPage.providerType_Suggestion_ListItem_ELM.replace("providerindex",
						Integer.toString(i + 1)));
				Assert.assertNotNull(findElements(ProviderPage.providerType_Suggestion_ListItems_ELM).get(i).getText());
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void selectProviderTypeFromAutoSuggestionList(String providerType, String eventType) {
		boolean selectProviderTypeFromSuggestionList = false;
		int suggestionList = 0;
		try {
			switch (eventType) {
			case "click event":
				click(ProviderPage.providerType_Select_ELM.replace("providertype", providerType));
				selectProviderTypeFromSuggestionList = true;
				break;
			case "tab key and enter key event":
				findElement(ProviderPage.providerType_TXT).sendKeys(Keys.TAB);
				suggestionList = getElementCount(ProviderPage.providerType_Suggestion_ListItems_ELM);
				logger.info("Suggestion List" + suggestionList);
				for (int i = 0; i < suggestionList; i++) {
					if (!selectProviderTypeFromSuggestionList) {
						logger.info(i + "->"
								+ findElements(ProviderPage.providerType_Suggestion_ListItems_ELM).get(i).getText());
						if (i == 0) {
							findElement(ProviderPage.providerType_Close_BTN).sendKeys(Keys.TAB);
						}
						logger.info("Highlight Provider Type::"
								+ findElements(ProviderPage.providerType_Suggestion_ListItem_Selected_ELM).get(0)
										.getText());
						logger.info(
								"Actual Provider Type:::" + getText(ProviderPage.providerType_Suggestion_ListItem_ELM
										.replace("providerindex", Integer.toString(i + 1))));
						if (findElements(ProviderPage.providerType_Suggestion_ListItem_Selected_ELM).get(0).getText()
								.equals(providerType)) {
							Assert.assertEquals(
									findElements(ProviderPage.providerType_Suggestion_ListItem_Selected_ELM).get(0)
											.getText(),
									getText(ProviderPage.providerType_Suggestion_ListItem_ELM.replace("providerindex",
											Integer.toString(i + 1))));
							findElement(ProviderPage.providerType_Suggestion_ListItem_ELM.replace("providerindex",
									Integer.toString(i + 1))).sendKeys(Keys.ENTER);
							selectProviderTypeFromSuggestionList = true;
						} else {
							if (getElementCount(ProviderPage.providerType_Suggestion_ListItems_ELM) != 1) {
								findElement(ProviderPage.providerType_Suggestion_ListItem_ELM.replace("providerindex",
										Integer.toString(i + 1))).sendKeys(Keys.TAB);
							}
						}
					}
				}
				break;
			case "keyboard arrow key and enter key event":
				findElement(ProviderPage.providerType_TXT).sendKeys(Keys.ARROW_DOWN);
				suggestionList = getElementCount(ProviderPage.providerType_Suggestion_ListItems_ELM);
				for (int i = 0; i < suggestionList; i++) {
					if (!selectProviderTypeFromSuggestionList) {
						logger.info(i + "->"
								+ findElements(ProviderPage.providerType_Suggestion_ListItems_ELM).get(i).getText());
						logger.info("Highlight Provider Type::"
								+ findElements(ProviderPage.providerType_Suggestion_ListItem_Selected_ELM).get(0)
										.getText());
						logger.info(
								"Actual Provider Type:::" + getText(ProviderPage.providerType_Suggestion_ListItem_ELM
										.replace("providerindex", Integer.toString(i + 1))));
						if (findElements(ProviderPage.providerType_Suggestion_ListItem_Selected_ELM).get(0).getText()
								.equals(providerType)) {
							Assert.assertEquals(
									findElements(ProviderPage.providerType_Suggestion_ListItem_Selected_ELM).get(0)
											.getText(),
									getText(ProviderPage.providerType_Suggestion_ListItem_ELM.replace("providerindex",
											Integer.toString(i + 1))));
							findElement(ProviderPage.providerType_Suggestion_ListItem_ELM.replace("providerindex",
									Integer.toString(i + 1))).sendKeys(Keys.ENTER);
							selectProviderTypeFromSuggestionList = true;
						} else {
							if (getElementCount(ProviderPage.providerType_Suggestion_ListItems_ELM) != 1) {
								findElement(ProviderPage.providerType_Suggestion_ListItem_ELM.replace("providerindex",
										Integer.toString(i + 1))).sendKeys(Keys.ARROW_DOWN);
							}
						}
					}
				}
				break;
			default:
				click(ProviderPage.providerType_Select_ELM.replace("providertype", providerType));
				break;
			}
			Assert.assertTrue(selectProviderTypeFromSuggestionList);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyProviderTypeFromAutoSuggestionList(String providerType) {
		boolean verifyProviderTypeFromSuggestionList = false;
		try {
			findElement(ProviderPage.providerType_TXT).sendKeys(Keys.TAB);
			int suggestionList = getElementCount(ProviderPage.providerType_Suggestion_ListItems_ELM);
			for (int i = 0; i < suggestionList; i++) {
				if (!verifyProviderTypeFromSuggestionList) {
					if (i == 0) {
						findElement(ProviderPage.providerType_Close_BTN).sendKeys(Keys.TAB);
					}
					if (getText(ProviderPage.providerType_Suggestion_ListItem_ELM.replace("providerindex",
							Integer.toString(i + 1))).equals(providerType)) {
						findElement(ProviderPage.providerType_Suggestion_ListItem_ELM.replace("providerindex",
								Integer.toString(i + 1)));
						verifyProviderTypeFromSuggestionList = true;
					} else {
						if (getElementCount(ProviderPage.providerType_Suggestion_ListItems_ELM) != 1) {
							findElement(ProviderPage.providerType_Suggestion_ListItem_ELM.replace("providerindex",
									Integer.toString(i + 1))).sendKeys(Keys.TAB);
						}
					}
				}
			}
			Assert.assertTrue(verifyProviderTypeFromSuggestionList);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifySelectedProviderType(String providerType) {
		try {
			Assert.assertEquals(getValue(ProviderPage.providerType_TXT), providerType);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyProviderTypeAutoSuggestion(int suggestionItemCount) {
		try {
			Assert.assertTrue(isElementDisplayed(ProviderPage.providerType_Suggestion_Listbox_ELM));
			Assert.assertEquals(getElementCount(ProviderPage.providerType_Suggestion_ListItems_ELM),
					suggestionItemCount);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyNoMatchProviderMessage(String message) {
		try {
			Assert.assertTrue(isElementDisplayed(ProviderPage.noProviderType_LBL));
			Assert.assertEquals(getText(ProviderPage.noProviderType_LBL), message);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyLinkInNoMatchProviderMessage(String link) {
		try {
			Assert.assertTrue(isElementDisplayed(ProviderPage.noProviderType_LNK));
			Assert.assertEquals(getText(ProviderPage.noProviderType_LNK), link);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyAutoSuggestionListClosed() {
		try {
			Assert.assertTrue(isElementNotPresent(ProviderPage.providerType_Suggestion_ListItems_ELM));
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyScreenDisplayedInProviderScreen(String labelName) {
		try {
			switch (labelName) {
			case "Select the provider type":
				Assert.assertTrue(isElementDisplayed(ProviderPage.select_The_ProviderOrServiceType_HLBL));
				break;
			case "Previously selected providers":
				Assert.assertTrue(isElementDisplayed(ProviderPage.previouslySelectedProviders_HLBL));
				break;
			default:
				Assert.assertFalse(true);
				throw new Exception("Not able to find the " + labelName + " label in provider screen in the list");
			}
		} catch (Exception e) {
			logger.error(labelName + " label is not displayed in provider screen");
			e.printStackTrace();
		}
	}

}