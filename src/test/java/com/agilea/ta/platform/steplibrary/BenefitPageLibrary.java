package com.agilea.ta.platform.steplibrary;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import com.agilea.ta.platform.framework.common.CommonLibrary;
import com.agilea.ta.platform.framework.common.CommonVariable;
import com.agilea.ta.platform.pageobject.BenefitPage;

import cucumber.api.DataTable;

public class BenefitPageLibrary extends CommonLibrary {

	private static Logger logger = LogManager.getLogger(BenefitPageLibrary.class);

	public static void verifyTextinSelectPlanScreen(String textVerify) {
		try {
			switch (textVerify) {
			case "Which benefit do you want to use?":
				Assert.assertTrue(isElementDisplayed(BenefitPage.whichBenefit_HLBL));
				logger.info(getText(BenefitPage.whichBenefit_HLBL));
				break;
			case "You don't have health benefits":
				Assert.assertTrue(isElementDisplayed(BenefitPage.noHealthBenefit_HLBL));
				Assert.assertEquals(BenefitPage.noHealthBenefit_H, getText(BenefitPage.noHealthBenefit_HLBL));
				break;
			case "Who is this claim for?":
				Assert.assertTrue(isElementDisplayed(BenefitPage.whoThisClaimFor_HLBL));
				break;
			default:
				throw new Exception("Not able to find the " + textVerify + " correct text in Select plan screen");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyPlanImage(String planImage, String displayValue) {
		String verifyPlanImage_ELM = null;
		String verifyPlan_ELM = null;
		try {
			switch (planImage) {
			case "Health":
				verifyPlan_ELM = BenefitPage.healthPlan_ELM;
				verifyPlanImage_ELM = BenefitPage.healthPlan_IMG;
				break;
			case "Dental":
				verifyPlanImage_ELM = BenefitPage.dentalPlan_IMG;
				break;
			case "Health care spending account":
				verifyPlanImage_ELM = BenefitPage.HealthcareSpendingAccountPlan_IMG;
				break;
			case "Wellness account":
				verifyPlanImage_ELM = BenefitPage.wellnessAccountPlan_IMG;
				break;
			default:
				throw new Exception("Not able to find the " + planImage + " correct plan image");
			}
			if (displayValue.equalsIgnoreCase("Should")) {
				Assert.assertTrue(isElementDisplayed(verifyPlan_ELM));
				Assert.assertTrue(isElementDisplayed(verifyPlanImage_ELM));
			} else {
				Assert.assertTrue(isElementNotPresent(verifyPlanImage_ELM));
			}
		} catch (Exception e) {
			if (displayValue.equalsIgnoreCase("Should")) {
				Assert.assertTrue(false);
			} else {
				Assert.assertTrue(true);
			}
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyPlanName(String planName, String displayValue) {
		String verifyPlanName_ELM = null;
		try {
			switch (planName) {
			case "Health":
				verifyPlanName_ELM = BenefitPage.healthPlan_LBL;
				break;
			case "Dental":
				verifyPlanName_ELM = BenefitPage.dentalPlan_LBL;
				break;
			case "Health care spending account":
				verifyPlanName_ELM = BenefitPage.HealthcareSpendingAccountPlan_LBL;
				break;
			case "Wellness account":
				verifyPlanName_ELM = BenefitPage.wellnessAccountPlan_LBL;
				break;
			default:
				throw new Exception("Not able to find the " + planName + " correct plan");
			}
			if (displayValue.equalsIgnoreCase("Should")) {
				Assert.assertTrue(isElementDisplayed(verifyPlanName_ELM));
			} else {
				Assert.assertTrue(isElementNotPresent(verifyPlanName_ELM));
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void selectPlan(String planName) {
		String selectPlan_ELM = null;
		try {
			switch (planName) {
			case "Health":
				selectPlan_ELM = BenefitPage.healthPlan_IMG;
				break;
			case "Dental":
				selectPlan_ELM = BenefitPage.dentalPlan_IMG;
				break;
			case "Health care spending account":
				selectPlan_ELM = BenefitPage.HealthcareSpendingAccountPlan_IMG;
				break;
			case "Wellness account":
				selectPlan_ELM = BenefitPage.wellnessAccountPlan_IMG;
				break;
			default:
				throw new Exception("Not able to find the " + planName + "  plan");
			}
			if (CommonVariable.breakPoint.equalsIgnoreCase("Mobile")) {
				click(BenefitPage.healthPlan_BTN);
			} else {
				actionClick(selectPlan_ELM);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyFamilyMemberList(int familyMemberList) {
		try {
			logger.info("Family Member List::" + findElements(BenefitPage.familyMemberList_ELM).size());
			Assert.assertEquals(findElements(BenefitPage.familyMemberList_ELM).size(), familyMemberList);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyFamilyMemberFirstAndLastName(DataTable familyMemberList) {
		try {
			List<Map<String, String>> familyMemberListMap = familyMemberList.asMaps(String.class, String.class);
			for (int i = 0; i < findElements(BenefitPage.familyMemberList_ELM).size(); i++) {
				Assert.assertTrue(isElementDisplayed(BenefitPage.familyMemberFirstAndLastName_LBL
						.replace("FAMILYMEMBERLIST", Integer.toString(i + 1))));
				Assert.assertTrue(getText(BenefitPage.familyMemberFirstAndLastName_LBL.replace("FAMILYMEMBERLIST",
						Integer.toString(i + 1))).length() != 0);
				Assert.assertEquals((familyMemberListMap.get(i).get("Family Member List")),
						getText(BenefitPage.familyMemberFirstAndLastName_LBL.replace("FAMILYMEMBERLIST",
								Integer.toString(i + 1))));
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void selectFamilyMemberRadioButton(String memberType, String memberName) {
		try {
			click(BenefitPage.familyMemberSelect_RADIO_BTN.replace("FAMILYMEMBER", memberName));
		} catch (Exception e) {
			logger.error("Not able to select " + memberType + "as " + memberName);
			e.printStackTrace();
		}
	}

	public static void selectFamilyMemberNameText(String memberType, String memberName) {
		try {
			click(BenefitPage.familyMemberNameSelect_Text_LBL.replace("FAMILYMEMBER", memberName));
		} catch (Exception e) {
			logger.error(e.getMessage());
			logger.error("Not able to select " + memberType + "as " + memberName + " through label");
			e.printStackTrace();
		}
	}

	public static void verifyFamilyMemberRadioButtonState(String memberName, String buttonState) {
		try {
			switch (buttonState) {
			case "not selected":
				Assert.assertFalse(findElement(
						BenefitPage.familyMemberFirstAndLastName_RADIO_BTN.replace("FAMILYMEMBERLIST", memberName))
								.isSelected());
				break;
			case "selected":
				Assert.assertTrue(findElement(
						BenefitPage.familyMemberFirstAndLastName_RADIO_BTN.replace("FAMILYMEMBERLIST", memberName))
								.isSelected());
				break;
			case "enabled":
				Assert.assertTrue(isElementDisplayed(
						BenefitPage.familyMemberFirstAndLastName_RADIO_BTN.replace("FAMILYMEMBERLIST", memberName)));
				Assert.assertFalse(findElement(
						BenefitPage.familyMemberFirstAndLastName_RADIO_BTN.replace("FAMILYMEMBERLIST", memberName))
								.isEnabled());
				break;
			default:
				Assert.assertFalse(true);
				throw new Exception("Input" + buttonState + "  is wrong");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyContinueButtonState(String continueButtonState) {
		try {
			switch (continueButtonState) {
			case "disabled":
				Assert.assertTrue(isElementDisplayed(BenefitPage.continue_BTN));
				Assert.assertFalse((findElement(BenefitPage.continue_BTN).isEnabled()));
				break;
			case "enabled":
				Assert.assertTrue(isElementDisplayed(BenefitPage.continue_BTN));
				Assert.assertTrue((findElement(BenefitPage.continue_BTN).isEnabled()));
				break;
			default:
				Assert.assertFalse(true);
				throw new Exception("Input" + continueButtonState + "  is wrong");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyPlanBenfitNotSelectedStatus(String planName) {
		String verifyPlanBenefit_ELM = null;
		try {
			switch (planName) {
			case "Health":
				verifyPlanBenefit_ELM = BenefitPage.healthPlan_ELM;
				break;
			case "Dental":
				verifyPlanBenefit_ELM = BenefitPage.dentalPlan_IMG;
				break;
			case "Health care spending account":
				verifyPlanBenefit_ELM = BenefitPage.HealthcareSpendingAccountPlan_IMG;
				break;
			case "Wellness account":
				verifyPlanBenefit_ELM = BenefitPage.wellnessAccountPlan_IMG;
				break;
			default:
				throw new Exception("Not able to find the " + planName + " benefit");
			}
			Assert.assertFalse(isElementSelected(verifyPlanBenefit_ELM));
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyPlanBenfitSelectedStatus(String planName) {
		String verifyPlanBenefit_ELM = null;
		try {
			switch (planName) {
			case "Health":
				verifyPlanBenefit_ELM = BenefitPage.healthPlan_ELM;
				break;
			case "Dental":
				verifyPlanBenefit_ELM = BenefitPage.dentalPlan_IMG;
				break;
			case "Health care spending account":
				verifyPlanBenefit_ELM = BenefitPage.HealthcareSpendingAccountPlan_IMG;
				break;
			case "Wellness account":
				verifyPlanBenefit_ELM = BenefitPage.wellnessAccountPlan_IMG;
				break;
			default:
				throw new Exception("Not able to find the " + planName + " benefit");
			}
			Assert.assertTrue(isElementEnabled(verifyPlanBenefit_ELM));
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void clicksButtonInBenefitScreen(String buttonName) {
		try {
			switch (buttonName) {
			case "continue":
				click(BenefitPage.continue_BTN);
				break;
			default:
				Assert.assertFalse(true);
				throw new Exception("Input" + buttonName + "  is wrong");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
}
