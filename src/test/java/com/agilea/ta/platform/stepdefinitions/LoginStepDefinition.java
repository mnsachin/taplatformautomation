package com.agilea.ta.platform.stepdefinitions;
import com.agilea.ta.platform.steplibrary.*;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginStepDefinition {
	
	
	@Given("^User is on login page$")
	public void user_is_on_login_page() {
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@When("^User enters correct username and password$")
	public void user_enters_correct_username_and_password() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@When("^User clicks on Sign In button$")
	public void user_clicks_on_Sign_In_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^verify that Home image is displayed on projects page$")
	public void verify_that_Home_image_is_displayed_on_projects_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^verify Add new test suite button is displayed$")
	public void verify_Add_new_test_suite_button_is_displayed() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^verify that error message is displayed$")
	public void verify_that_error_message_is_displayed() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

}
