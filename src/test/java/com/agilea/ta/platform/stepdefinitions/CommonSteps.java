package com.agilea.ta.platform.stepdefinitions;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.agilea.ta.platform.framework.builder.WebCapabilitiesBuilder;
import com.agilea.ta.platform.framework.common.CommonLibrary;
import com.agilea.ta.platform.framework.common.CommonVariable;
import com.agilea.ta.platform.framework.factory.WebDriverFactory;
import com.agilea.ta.platform.steplibrary.CommonPageLibrary;
import com.vimalselvam.cucumber.listener.Reporter;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CommonSteps {
	protected static WebDriver driver;
	protected DesiredCapabilities caps;
	private static Logger logger = LogManager.getLogger(CommonSteps.class);
	protected static Properties prop;
	public CommonVariable commonVariables;

	@Before
	public void beforeScenario(Scenario scenario) throws Exception {
		logger.info("Automation Execution start method");
		logger.info("Executing Scenario Name:" + scenario.getName());
		caps = new WebCapabilitiesBuilder().addBrowser(CommonVariable.browserName)
				.addBrowserDriverExecutablePath(
						System.getProperty("user.dir") + File.separator + CommonVariable.driverPath)
				.addVersion(CommonVariable.version).addPlatform(CommonVariable.platform).build();
		driver = new WebDriverFactory().createDriver(caps);
		CommonLibrary.openUrl(CommonVariable.url);
		if (CommonVariable.breakPoint.equalsIgnoreCase("DESKTOP")
				&& CommonVariable.maximizeBrowser.equalsIgnoreCase("Yes")) {
			logger.info("Running Automation in Desktop " + CommonVariable.browserName
					+ " Browser with maximize browser size");
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
		} else if (CommonVariable.breakPoint.equalsIgnoreCase("MOBILE")) {
			logger.info("Running Automation in Mobile " + CommonVariable.browserName + " Browser");
		}
	}

	@After
	public void afterScenario(Scenario scenario) throws InterruptedException, IOException {
		if (scenario.isFailed()) {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			File destinationPath = new File(CommonVariable.currentRootDir + File.separator + "target" + File.separator
					+ "screenshots" + File.separator + scenario.getName() + ".png");
			FileUtils.copyFile(scrFile, destinationPath);
			scenario.embed(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES), "image/png");
			Reporter.addScreenCaptureFromPath(destinationPath.toString());
		}
		if (!CommonVariable.browserName.equalsIgnoreCase("firefox")) {
			driver.close();
		}
		driver.quit();
	}

	@Then("verifies \"([^\"]*)\" progress navigation bar label \"([^\"]*)\" displayed$")
	public void verifies_Progress_Navigation_Bar_Label_Displayed(String progressNavigationBarLabel,
			String labelStatus) {
		CommonPageLibrary.verifyProgressNavigationBarLabel(progressNavigationBarLabel, labelStatus);
	}

	@Then("verifies \"([^\"]*)\" progress navigation bar label is \"([^\"]*)\"$")
	public void verifies_Progress_Navigation_Bar_Label(String progressNavigationBarLabel, String labelStyle) {
		CommonPageLibrary.verifyProgressNavigationBarText(progressNavigationBarLabel, labelStyle);
	}

	@Then("verifies step (\\d+) progress navigation bar is \"([^\"]*)\"$")
	public void verifies_Step_Progress_Navigation_Bar(int stepProgressNavigationBar, String labelStyle) {
		CommonPageLibrary.verifyStepProgressNavigationBars(stepProgressNavigationBar, labelStyle);
	}

	@Then("verifies progress navigation bar after \"([^\"]*)\"$")
	public void verifies_Progress_Navigation_Bar(String screenName) {
		CommonPageLibrary.verifyProgressNavigationBar(screenName);
	}

	@Given("^plan member is on temporary login page$")
	public void plan_Member_Is_On_temporary_Home_Page() {
		CommonPageLibrary.verifyLoginScreen();
	}

	@When("^plan member enters MemberID \"([^\"]*)\" in temporary login page$")
	public void plan_Member_Enters_MemberID_In_Temporary_Login_Page(String memberID) throws Throwable {
		CommonPageLibrary.entersMemberId(memberID);
	}

	@When("^clicks \"([^\"]*)\" button in temporary login page$")
	public void clicks_Button_In_Temporary_Login_Page(String buttonName) {
		CommonPageLibrary.clickButtonInTemporaryLoginPage(buttonName);
	}

	@Then("^verifies Manulife logo is displayed in \"([^\"]*)\" screen$")
	public void verifies_Manulife_Logo_Is_Displayed_In_Screen(String screenName) {
		CommonPageLibrary.verifyManulifeLogoScreen(screenName);
	}

	@When("user clicks on step (\\d+) tick mark$")
	public void user_Clicks_On_Step_Tick_Mark(int stepNumber) {
		CommonPageLibrary.clicksOnNavigationBarStepNumberTickMark(stepNumber);
	}

	@When("user clicks on step (\\d+) completed \"([^\"]*)\" text$")
	public void user_Clicks_On_Step_Completed_Text(int stepNumber, String navigationBarText) {
		CommonPageLibrary.clicksOnNavigationBarCompletedStepText(stepNumber, navigationBarText);
	}
	
	@When("user clicks on step (\\d+) active \"([^\"]*)\" text$")
	public void user_Clicks_On_Step_Active_Text(int stepNumber, String navigationBarText) {
		CommonPageLibrary.clicksOnNavigationBarActiveStepText(stepNumber, navigationBarText);
	}
	
	@When("user clicks on step (\\d+) active status$")
	public void user_Clicks_On_Step_Active_Status(int stepNumber) {
		CommonPageLibrary.clicksOnNavigationBarStepNumberActiveStatus(stepNumber);
	}
}
