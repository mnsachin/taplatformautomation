package com.agilea.ta.platform.framework.browserProfile;

public abstract class BrowserProfile {
	public abstract Object createProfile();
}
