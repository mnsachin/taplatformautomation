package com.agilea.ta.platform.steplibrary;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import com.agilea.ta.platform.framework.common.CommonLibrary;
import com.agilea.ta.platform.framework.common.CommonVariable;
import com.agilea.ta.platform.pageobject.CommonPage;

public class CommonPageLibrary extends CommonLibrary {

	private static Logger logger = LogManager.getLogger(CommonPageLibrary.class);

	public static void verifyLoginScreen() {
		try {
			Assert.assertTrue(isElementDisplayed(CommonPage.login_HLBL));
			Assert.assertTrue(isElementDisplayed(CommonPage.temporaryLogin_MSG));
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyManulifeLogoScreen(String screenName) {
		try {
			Assert.assertTrue(isElementDisplayed(CommonPage.manulifeLogo_IMG));
		} catch (Exception e) {
			logger.error("Manulife Logo is not displayed in " + screenName + " screen");
			e.printStackTrace();
		}
	}

	public static void verifyProgressNavigationBarLabel(String progressNavigationBarLabel, String labelStatus) {
		try {
			String progressNavigationBars_ELM = null;
			switch (progressNavigationBarLabel) {
			case "Benefit":
				if (CommonVariable.breakPoint.equalsIgnoreCase("Mobile")) {
					progressNavigationBars_ELM = CommonPage.benefit_Mobile_ELM;
				} else {
					progressNavigationBars_ELM = CommonPage.benefit_ELM;
				}
				break;
			case "Provider":
				if (CommonVariable.breakPoint.equalsIgnoreCase("Mobile")) {
					progressNavigationBars_ELM = CommonPage.provider_Mobile_ELM;
				} else {
					progressNavigationBars_ELM = CommonPage.provider_ELM;
				}
				break;
			case "Expenses":
				if (CommonVariable.breakPoint.equalsIgnoreCase("Mobile")) {
					progressNavigationBars_ELM = CommonPage.expenses_Mobile_ELM;
				} else {
					progressNavigationBars_ELM = CommonPage.expenses_ELM;
				}
				break;
			case "Submit":
				if (CommonVariable.breakPoint.equalsIgnoreCase("Mobile")) {
					progressNavigationBars_ELM = CommonPage.submit_Mobile_ELM;
				} else {
					progressNavigationBars_ELM = CommonPage.submit_ELM;
				}
				break;
			default:
				Assert.assertFalse(true);
				throw new Exception(
						"Not able to find the " + progressNavigationBarLabel + " navigation bar in the list");
			}
			if (labelStatus.equalsIgnoreCase("is")) {
				Assert.assertTrue(isElementDisplayed(progressNavigationBars_ELM));
			} else if (labelStatus.equalsIgnoreCase("is not")) {
				Assert.assertTrue(isElementNotPresent(progressNavigationBars_ELM));
			}
		} catch (Exception e) {
			logger.error("Not able to find the " + progressNavigationBarLabel
					+ " progress navigation bar when plan member in the screen");
			e.printStackTrace();
		}
	}

	public static void verifyProgressNavigationBarText(String progressNavigationBarLabel, String labelStyle) {
		try {
			String progressNavigationBarLabel_ELM = null;
			switch (progressNavigationBarLabel) {
			case "Benefit":
				if (labelStyle.equals("bolded")) {
					if (CommonVariable.breakPoint.equalsIgnoreCase("Mobile")) {
						progressNavigationBarLabel_ELM = CommonPage.benefit_Mobile_ELM;
					} else {
						progressNavigationBarLabel_ELM = CommonPage.benefit_Bolded_ELM;
					}
				} else if (labelStyle.equals("completed and not bolded")) {
					if (CommonVariable.breakPoint.equalsIgnoreCase("Mobile")) {
						progressNavigationBarLabel_ELM = CommonPage.benefit_Mobile_ELM;
					} else {
						progressNavigationBarLabel_ELM = CommonPage.benefit_CompletedAndNotBolded_ELM;
					}
				} else {
					progressNavigationBarLabel_ELM = CommonPage.benefit_NotBolded_ELM;
				}
				break;
			case "Provider":
				if (labelStyle.equals("bolded")) {
					if (CommonVariable.breakPoint.equalsIgnoreCase("Mobile")) {
						progressNavigationBarLabel_ELM = CommonPage.provider_Mobile_ELM;
					} else {
						progressNavigationBarLabel_ELM = CommonPage.provider_Bolded_ELM;
					}
				} else if (labelStyle.equals("completed and not bolded")) {
					if (CommonVariable.breakPoint.equalsIgnoreCase("Mobile")) {
						progressNavigationBarLabel_ELM = CommonPage.provider_Mobile_ELM;
					} else {
						progressNavigationBarLabel_ELM = CommonPage.provider_CompletedAndNotBolded_ELM;
					}
				} else {
					progressNavigationBarLabel_ELM = CommonPage.provider_NotBolded_ELM;
				}
				break;
			case "Expenses":
				if (labelStyle.equals("bolded")) {
					if (CommonVariable.breakPoint.equalsIgnoreCase("Mobile")) {
						progressNavigationBarLabel_ELM = CommonPage.expenses_Mobile_ELM;
					} else {
						progressNavigationBarLabel_ELM = CommonPage.expenses_Bolded_ELM;
					}
				} else if (labelStyle.equals("completed and not bolded")) {
					if (CommonVariable.breakPoint.equalsIgnoreCase("Mobile")) {
						progressNavigationBarLabel_ELM = CommonPage.expenses_Mobile_ELM;
					} else {
						progressNavigationBarLabel_ELM = CommonPage.expenses_CompletedAndNotBolded_ELM;
					}
				} else {
					progressNavigationBarLabel_ELM = CommonPage.expenses_NotBolded_ELM;
				}
				break;
			case "Submit":
				if (labelStyle.equals("bolded")) {
					if (CommonVariable.breakPoint.equalsIgnoreCase("Mobile")) {
						progressNavigationBarLabel_ELM = CommonPage.submit_Mobile_ELM;
					} else {
						progressNavigationBarLabel_ELM = CommonPage.submit_Bolded_ELM;
					}
				} else if (labelStyle.equals("completed and not bolded")) {
					if (CommonVariable.breakPoint.equalsIgnoreCase("Mobile")) {
						progressNavigationBarLabel_ELM = CommonPage.submit_Mobile_ELM;
					} else {
						progressNavigationBarLabel_ELM = CommonPage.submit_CompletedAndNotBolded_ELM;
					}
				} else {
					progressNavigationBarLabel_ELM = CommonPage.submit_NotBolded_ELM;
				}
				break;
			default:
				Assert.assertFalse(true);
				throw new Exception(
						"Not able to find the " + progressNavigationBarLabel + " navigation bar in the list");
			}
			Assert.assertTrue(isElementDisplayed(progressNavigationBarLabel_ELM));
		} catch (Exception e) {
			logger.error("Not able to find the " + progressNavigationBarLabel
					+ " progress navigation bar when plan member in the screen");
			e.printStackTrace();
		}
	}

	public static void verifyStepProgressNavigationBars(int stepProgressNavigationBarLabel, String labelStyle) {
		try {
			String stepProgressNavigationBars_ELM = null;
			String verifyStepNumberNotPresent = null;
			switch (stepProgressNavigationBarLabel) {
			case 1:
				if (labelStyle.equals("highlighted")) {
					stepProgressNavigationBars_ELM = CommonPage.step1_Highlighted_ELM;
				} else if (labelStyle.equals("completed and showed tick mark instead of step number")) {
					stepProgressNavigationBars_ELM = CommonPage.step1_TickMark_ELM;
					verifyStepNumberNotPresent = CommonPage.step1_Completed_ELM;
				} else {
					stepProgressNavigationBars_ELM = CommonPage.step1_NotHighlighted_ELM;
				}
				break;
			case 2:
				if (labelStyle.equals("highlighted")) {
					stepProgressNavigationBars_ELM = CommonPage.step2_Highlighted_ELM;
				} else if (labelStyle.equals("completed and showed tick mark instead of step number")) {
					stepProgressNavigationBars_ELM = CommonPage.step2_TickMark_ELM;
					verifyStepNumberNotPresent = CommonPage.step2_Completed_ELM;
				} else {
					stepProgressNavigationBars_ELM = CommonPage.step2_NotHighlighted_ELM;
				}
				break;
			case 3:
				if (labelStyle.equals("highlighted")) {
					stepProgressNavigationBars_ELM = CommonPage.step3_Highlighted_ELM;
				} else if (labelStyle.equals("completed and showed tick mark instead of step number")) {
					stepProgressNavigationBars_ELM = CommonPage.step3_TickMark_ELM;
					verifyStepNumberNotPresent = CommonPage.step3_Completed_ELM;
				} else {
					stepProgressNavigationBars_ELM = CommonPage.step3_NotHighlighted_ELM;
				}
				break;
			case 4:
				if (labelStyle.equals("highlighted")) {
					stepProgressNavigationBars_ELM = CommonPage.step4_Highlighted_ELM;
				} else if (labelStyle.equals("completed and showed tick mark instead of step number")) {
					stepProgressNavigationBars_ELM = CommonPage.step4_TickMark_ELM;
					verifyStepNumberNotPresent = CommonPage.step4_Completed_ELM;
				} else {
					stepProgressNavigationBars_ELM = CommonPage.step4_NotHighlighted_ELM;
				}
				break;
			default:
				Assert.assertFalse(true);
				throw new Exception("Not able to find the " + stepProgressNavigationBarLabel
						+ " step progress navigation bar in the list");
			}
			if (labelStyle.equals("completed and showed tick mark instead of step number")) {
				Assert.assertFalse(isElementDisplayed(verifyStepNumberNotPresent));
				Assert.assertTrue(isElementDisplayed(stepProgressNavigationBars_ELM));
			} else
				Assert.assertTrue(isElementDisplayed(stepProgressNavigationBars_ELM));
		} catch (Exception e) {
			logger.error("Not able to find the " + stepProgressNavigationBarLabel
					+ " navigation bar when plan member in the screen");
			e.printStackTrace();
		}
	}

	public static void entersMemberId(String memberId) {
		try {
			type(CommonPage.memberId_TXT, memberId);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void clickButtonInTemporaryLoginPage(String buttonName) {
		try {
			if (CommonVariable.breakPoint.equalsIgnoreCase("Mobile")
					&& CommonVariable.userAgent.equalsIgnoreCase("Yes")) {
				Thread.sleep(5000);
			}
			click(CommonPage.setMemberId_BTN);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void verifyProgressNavigationBar(String screenName) {
		try {
			String progressNavigationBar_ELM = null;
			switch (screenName) {
			case "Benefit":
				progressNavigationBar_ELM = CommonPage.progressBar_1_ELM;
				break;
			case "Provider":
				progressNavigationBar_ELM = CommonPage.progressBar_2_ELM;
				break;
			case "Expenses":
				progressNavigationBar_ELM = CommonPage.progressBar_3_ELM;
				break;
			default:
				Assert.assertFalse(true);
				throw new Exception("Not able to find the " + screenName + " screen");
			}
			Assert.assertTrue(isElementDisplayed(progressNavigationBar_ELM));
		} catch (Exception e) {
			logger.error(
					"Not able to find the progress navigation bar when plan member in the " + screenName + " screen");
			e.printStackTrace();
		}
	}

	public static void clicksOnNavigationBarStepNumberTickMark(int stepNumber) {
		try {
			switch (stepNumber) {
			case 1:
				click(CommonPage.step1_TickMark_ELM);
				break;
			case 2:
				click(CommonPage.step2_TickMark_ELM);
				break;
			case 3:
				click(CommonPage.step3_TickMark_ELM);
				break;
			case 4:
				click(CommonPage.step4_TickMark_ELM);
				break;
			default:
				Assert.assertFalse(true);
				throw new Exception("Not able to find the step " + stepNumber + " tick mark progress navigation bar in the list");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void clicksOnNavigationBarStepNumberActiveStatus(int stepNumber) {
		try {
			switch (stepNumber) {
			case 1:
				click(CommonPage.step1_Highlighted_ELM);
				break;
			case 2:
				click(CommonPage.step2_Highlighted_ELM);
				break;
			case 3:
				click(CommonPage.step3_Highlighted_ELM);
				break;
			case 4:
				click(CommonPage.step4_Highlighted_ELM);
				break;
			default:
				Assert.assertFalse(true);
				throw new Exception("Not able to find the step " + stepNumber + " active status progress navigation bar in the list");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public static void clicksOnNavigationBarCompletedStepText(int stepNumber, String navigationBarText) {
		try {
			switch (navigationBarText.toLowerCase()) {
			case "benefit":
				click(CommonPage.benefit_CompletedAndNotBolded_ELM);
				break;
			case "provider":
				click(CommonPage.provider_CompletedAndNotBolded_ELM);
				break;
			case "expense":
				click(CommonPage.expenses_CompletedAndNotBolded_ELM);
				break;
			case "submit":
				click(CommonPage.submit_CompletedAndNotBolded_ELM);
				break;
			default:
				Assert.assertFalse(true);
				throw new Exception("Not able to find the step " + stepNumber + navigationBarText
						+ " text progress navigation bar in the list");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void clicksOnNavigationBarActiveStepText(int stepNumber, String navigationBarText) {
		try {
			switch (navigationBarText.toLowerCase()) {
			case "benefit":
				click(CommonPage.benefit_Bolded_ELM);
				break;
			case "provider":
				click(CommonPage.provider_Bolded_ELM);
				break;
			case "expense":
				click(CommonPage.expenses_Bolded_ELM);
				break;
			case "submit":
				click(CommonPage.submit_Bolded_ELM);
				break;
			default:
				Assert.assertFalse(true);
				throw new Exception("Not able to find the step " + stepNumber + navigationBarText
						+ " text progress navigation bar in the list");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
}
