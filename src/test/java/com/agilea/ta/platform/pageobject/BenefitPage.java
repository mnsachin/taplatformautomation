package com.agilea.ta.platform.pageobject;

public class BenefitPage {
	
	//Plan
	public static String whichBenefit_HLBL = "//h2[contains(text(),'Which benefit do you want to use?')]~XPATH";
	public static String healthPlan_ELM = "//div[@class='three-state-icon-content']//div[@class='col-lg-3 col-sm-6 col-md-6 col-xs-6 icon-box']~XPATH";
	public static String healthPlan_IMG = "circle~CSS";
	public static String healthPlan_BTN = "//div[@class='col-lg-3 col-sm-6 col-md-6 col-xs-6 icon-box']/div~XPATH";
	public static String healthPlan_LBL = "//font[contains(text(),'health')]~XPATH";
	public static String dentalPlan_IMG = "//div[@class='sc-kxynE kjMSUd']~XPATH";
	public static String dentalPlan_LBL = "//div[contains(text(),'Dental')]~XPATH";
	public static String HealthcareSpendingAccountPlan_IMG = "//div[@class='sc-kxynE kjMSUd']~XPATH";
	public static String HealthcareSpendingAccountPlan_LBL = "//div[contains(text(),'Health care Spending Account')]~XPATH";
	public static String wellnessAccountPlan_IMG = "//div[@class='sc-kxynE kjMSUdsss']~XPATH";
	public static String wellnessAccountPlan_LBL = "//div[contains(text(),'Wellness Account')]~XPATH";
    public static String noHealthBenefit_HLBL = "//h5[@class='card-title']~XPATH";
	public static String noHealthBenefit_H = "You don't have health benefits";
	
	//Family Member
	public static String whoThisClaimFor_HLBL = "//h2[contains(text(),'Who is this claim for?')]~XPATH";
	public static String familyMemberList_ELM = "//ul[@class='list-group']/li~XPATH";
	public static String familyMemberFirstAndLastName_LBL = "//ul[@class='list-group']/li[FAMILYMEMBERLIST]/label~XPATH";
	public static String familyMemberFirstAndLastName_RADIO_BTN = "//ul[@class='list-group']//label[contains(text(),'FAMILYMEMBERLIST')]/input~XPATH";
	public static String familyMemberSelect_RADIO_BTN ="//ul[@class='list-group']//label[contains(text(),'FAMILYMEMBER')]//span~XPATH";
	public static String familyMemberNameSelect_Text_LBL ="//ul[@class='list-group']//label[contains(text(),'FAMILYMEMBER')]~XPATH";
	public static String continue_BTN = "//div[@class='buttons-Container']//button[contains(text(),'Continue')]~XPATH";
       
}
