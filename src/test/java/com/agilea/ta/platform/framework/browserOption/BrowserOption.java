package com.agilea.ta.platform.framework.browserOption;

public abstract class BrowserOption {
	public abstract void setHeadless(boolean value);
}
