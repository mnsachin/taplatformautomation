package com.agilea.ta.platform.steplibrary;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.agilea.ta.platform.framework.common.CommonLibrary;
import com.agilea.ta.platform.framework.common.CommonVariable;
import com.agilea.ta.platform.pageobject.LoginPage;

import cucumber.api.DataTable;

public class LoginLibrary  extends CommonLibrary{

		private static Logger logger = LogManager.getLogger(LoginLibrary.class);

		public static void enterTextOnUserNameField(String userName) {
			try {
				//type(LoginPage.userNameTxt, userName);
				driver.findElement(By.xpath(LoginPage.userNameTxt)).sendKeys(userName);
			} catch (Exception e) {
				logger.error(e.getMessage());
				e.printStackTrace();
			}
		}
	
	
}
