@Login
Feature:  TestAssure Login Feature

  @Smoke @Regression 
  Scenario: Login to TestAssure with correct credentials

    Given User is on login page
    When User enters correct username and password
    And User clicks on Sign In button
    Then verify that Home image is displayed on projects page
    And verify Add new test suite button is displayed
    
      @Smoke @Regression 
  Scenario: Login with incorrect credentials

    Given User is on login page
    When User enters correct username and password
    And User clicks on Sign In button
    Then verify that error message is displayed
    And verify Add new test suite button is displayed