package com.agilea.ta.platform.pageobject;

public class LoginPage {
	
	public static String userNameTxt = "//input[@id='email']";
	public static String passwordTxt ="//input[@id='password']~XPATH"; 
	public static String signinBtn = "//button[@id='signIn']~XPATH";

}
